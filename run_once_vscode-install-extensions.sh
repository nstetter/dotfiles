#! /bin/bash

EXTENSIONS=(
    chrisant996.autotrim \
    donjayamanne.githistory \
    eamodio.gitlens \
    hashicorp.terraform \
    jakearl.search-editor-apply-changes \
    mhutchie.git-graph \
    ms-azuretools.vscode-docker \
    ms-kubernetes-tools.vscode-kubernetes-tools \
    ms-vscode-remote.remote-ssh \
    ms-vscode-remote.remote-ssh-edit \
    PKief.material-icon-theme \
    redhat.vscode-yaml \
    stackbreak.comment-divider \
    vscodevim.vim \
    VSpaceCode.whichkey
)

if type -p code &> /dev/null;
then
    for EXTENSION in ${EXTENSIONS[@]}
    do
        code --install-extension $EXTENSION
    done
else
    echo "VS Code not installed, cannot install extensions"
fi
