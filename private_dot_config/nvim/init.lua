-- -------------------------------------------------------------------------- --
--                                  Built-Ins                                 --
-- -------------------------------------------------------------------------- --

-- --------------------------------- Basics --------------------------------- --
vim.opt.encoding = "utf-8" -- Set encoding to utf-8

vim.opt.shell = "zsh" -- Set the shell to zsh

vim.opt.fillchars:append { vert = '│' } -- Set fillchars for vertical split
-- ▸, ❯, ❮, ⎵
vim.opt.listchars = { tab = '│ ', trail = '·', extends = '❯', precedes = '❮', nbsp = '⎵' } -- Set listchars

vim.opt.clipboard = "unnamedplus,unnamed" -- Configure clipboard settings to use both PRIMARY and SYSTEM clipboards

vim.opt.ignorecase = true -- Ignore case when searching
vim.opt.smartcase = true --but use smartcase if there are mixed case characters

vim.opt.hidden = true -- Enable hidden buffers

vim.opt.laststatus = 2 -- Always show the status line

vim.opt.showmode = false -- Don't show mode in the command line (done by plugins like Lightline)

vim.opt.smartindent = true -- Enable smart indenting

vim.opt.diffopt:append {'iwhite'} -- Ignore whitespace in diffs

vim.opt.updatetime = 200
vim.opt.timeoutlen = 500
vim.opt.ttimeoutlen = 25

-- Highlight the 80th column with a different background color
vim.cmd("highlight ColorColumn ctermbg=236")
vim.opt.colorcolumn = "80"

vim.g.mapleader = " " -- Map <Space> as leader key

vim.opt.mouse = "a" -- Enable mouse support for selecting text

vim.opt.concealcursor = "c" -- Disable conceal for the current line

-- Auto switch to normal mode when switching buffers (except for terminal buffers)
vim.api.nvim_create_autocmd({ "BufEnter"}, {
    pattern = "*",
    callback = function()
        if vim.bo.buftype ~= "terminal" then
            vim.cmd "stopinsert"
        end
    end,
})

-- Define a command :Q as a synonym for :q with optional bang (!)
vim.cmd([[command -bang Q q<bang>]])

vim.opt.inccommand = "nosplit" -- Preview replace/substitute as you type

-- Set defaults; filetype-specific settings are handled by vim-polyglot
vim.opt.tabstop = 4 -- The width of a TAB is set to 4. It is still a \t, but Neovim will interpret it to have a width of 4.
vim.opt.shiftwidth = 4 -- When indenting with '>', use 4 spaces width
vim.opt.softtabstop = 4 -- Sets the number of columns for a TAB
vim.opt.expandtab = true -- Expand TABs to spaces


-- ------------------------------ Line Numbers ------------------------------ --
-- Automatic toggling between line number modes
-- Set number mode when entering a buffer, except if type "nofile"

vim.opt.number = true

local augroup = vim.api.nvim_create_augroup("numbertoggle", { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "FocusGained", "InsertLeave" }, {
   pattern = "*",
   group = augroup,
   callback = function()
      if vim.bo.buftype ~= "nofile" and vim.api.nvim_get_mode().mode ~= "i" then
    --   if vim.bo.buftype ~= "nofile" and vim.o.nu then
         vim.opt.relativenumber = true
      end
   end,
})
vim.api.nvim_create_autocmd({ "BufLeave", "FocusLost", "InsertEnter" }, {
   pattern = "*",
   group = augroup,
   callback = function()
      if vim.bo.buftype ~= "nofile" then
    --   if vim.bo.buftype ~= "nofile" and vim.o.nu then
        --  vim.opt.number = true
         vim.opt.relativenumber = false
        --  vim.cmd "redraw"
      end
   end,
})

-- Turn off line numbers in terminal
local augroup = vim.api.nvim_create_augroup("termnumberoff", { clear = true })
vim.api.nvim_create_autocmd({ "TermOpen" }, {
   pattern = "*",
   group = augroup,
   callback = function()
      vim.opt_local.number = false
      vim.opt_local.relativenumber = false
   end,
})
vim.api.nvim_create_autocmd({ "BufEnter", "FocusGained", "InsertLeave" }, {
   pattern = "*",
   group = augroup,
   callback = function()
      if vim.bo.buftype == "terminal" then
         vim.opt.number = false
         vim.opt.relativenumber = false
      end
   end,
})


-- --------------------------------- Folding -------------------------------- --
vim.wo.foldmethod = 'syntax'     -- Fold based on syntax
vim.wo.foldnestmax = 2           -- Deepest fold
vim.wo.foldenable = false        -- Don't fold by default
vim.wo.foldlevel = 1             -- Level when folding whole buffer (zm/zr)

-- Highlight Folded lines
vim.cmd('highlight Folded ctermfg=252')  -- Fold line foreground
vim.cmd('highlight Folded ctermbg=236')  -- Fold line background


-- -------------------------------- Terminal -------------------------------- --

-- bind ESC,ESC to normal mode in term
-- vim.keymap.set('t', '<Esc><Esc>', '<C-\\><C-n>', { noremap = true })

-- When term starts, auto go into insert mode
vim.api.nvim_create_autocmd({ "TermOpen" }, {
   pattern = "*",
   callback = function()
      vim.cmd "startinsert"
   end,
})
vim.api.nvim_create_autocmd({ "BufEnter" }, {
   pattern = "*",
   callback = function()
      if vim.bo.buftype == "terminal" then
         vim.cmd "startinsert"
      end
   end,
})


-- ------------------------------ QuickFixList ------------------------------ --
-- ToggleQuickFixList
vim.api.nvim_create_user_command('ToggleQuickfixList',
    function(input)
        local function GetBufferList()
            local buflist = {}
            for _, bufnr in ipairs(vim.fn.getbufinfo()) do
            table.insert(buflist, bufnr.name)
            end
            return buflist
        end

        local buflist = GetBufferList()
        for _, bufname in ipairs(buflist) do
            if vim.fn.match(bufname, "Quickfix List") ~= -1 then
            local bufnum = vim.fn.bufnr(bufname)
            if vim.fn.bufwinnr(bufnum) ~= -1 then
                vim.cmd("cclose")
                return
            end
            end
        end

        local winnr_before = vim.fn.winnr()
        vim.cmd("copen")
        if vim.fn.winnr() ~= winnr_before then
            vim.cmd("wincmd p")
        end
    end,
    {desc = 'Toggle Quickfix List'}
)

-- --------------------------------- KeyMaps -------------------------------- --
-- Make Y behave like D and C instead of like yy
vim.keymap.set('n', 'Y', 'y$', { noremap = true })

-- Clean search highlighting with ESC
vim.keymap.set('n', '<esc>', ':nohlsearch<CR>', { noremap = true, silent = true })

-- Auto indent on paste
vim.keymap.set('n', 'p', 'p=`]', { noremap = true })

-- Allow arrow keys in file browser
local augroup = vim.api.nvim_create_augroup("netrw", { clear = true })
vim.api.nvim_create_autocmd({ "FileType" }, {
   pattern = "netrw",
   group = augroup,
   callback = function()
        vim.keymap.set('n', '<Up>', '<Up>', { noremap = true, buffer = true })
   end,
})
vim.api.nvim_create_autocmd({ "FileType" }, {
   pattern = "netrw",
   group = augroup,
   callback = function()
        vim.keymap.set('n', '<Down>', '<Down>', { noremap = true, buffer = true })
   end,
})



-- -------------------------------------------------------------------------- --
--                                   Plugins                                  --
-- -------------------------------------------------------------------------- --

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    "navarasu/onedark.nvim",
    "voldikss/vim-floaterm",
    "Yggdroot/indentLine",
    "sheerun/vim-polyglot",
    "airblade/vim-gitgutter",
    "rhysd/git-messenger.vim",
    { "folke/which-key.nvim",
        lazy = true,
        opts = {
            window = {
                border = "single", -- none, single, double, shadow
                position = "bottom", -- bottom, top
                margin = { 1, 0.1, 0.05, 0.1 }, -- extra window margin [top, right, bottom, left]. When between 0 and 1, will be treated as a percentage of the screen size.
            }
          },
    },
    { "nvim-lualine/lualine.nvim",
        dependencies = {
            "kyazdani42/nvim-web-devicons",
        },
    },
    { "junegunn/fzf.vim",
        dependencies = { 'junegunn/fzf', build = { ':call fzf#install()' }}
    }
})


-- ---------------------------------- Theme --------------------------------- --

require('onedark').setup {
    style = 'dark'
}
require('onedark').load()


-- --------------------------------- lualine -------------------------------- --

require('lualine').setup()

-- -------------------------------- which-key ------------------------------- --
local wk = require("which-key")

wk.register({
    s = { ":update", "Save File" },
    l = { ":BLines", "Lines in the current buffer" },
    L = { ":Lines", "Lines in loaded buffers" },
    b = { ":Buffers", "Open buffers" },
    h = { ":Helptags", "Helptags" },
    e = { ":CocCommand explorer", "Open/Focus Explorer" },
    ee = { ":CocCommand explorer --toggle", "Toggle Explorer" },
    ww = { ":VimwikiIndex", "VimWiki Index" },
    wt = { ":VimwikiToggleListItem", "VimWiki Toggle Item" },
    wf = { ":NV", "Vimwiki FZF" },

    g = {
        name = "+git",
        c = { ":BCommits", "Commits for current buffer" },
        C = { ":Commits", "Commits for current tree" },
        f = { ":GFiles?", "Git files (git status)" },
        b = { "<Plug>(git-messenger)", "Git Blame for current line" },
        l = { ":FTlazygit", "Toggle lazygit window" },
        t = { ":FTtig", "Toggle tig window" },
        n = { ":GitGutterNextHunk", "Go to next hunk" },
        p = { ":GitGutterPrevHunk", "Go to previous hunk" },
        s = { ":GitGutterStageHunk", "Stage Hunk" },
        v = { ":GitGutterPreviewHunk", "Preview Hunk" },
        u = { ":GitGutterUndoHunk", "Undo Hunk" },
        d = { ":Gvdiff", "Start git diff" },
        dc = { ":GitNextConflict", "Next conflicted file" },
        du = { "diffget //2", "diffget upstream version" },
        dl = { "diffget //3", "diffget local version" },
    },

    f = {
        name = "+files",
        f = { ":Files", "Files in workdir" },
        i = { ':Files <C-r>=expand("%:h")<CR>/', 'Files in current dir' },
        r = { ":FZFMru", "Most recent files" },
        h = { ":History", "v:oldfiles and open buffers" },
        H = { ":FZFMru", "Most recent files" },
        d = { ":FTranger", "Run ranger" },
        b = { ":FTbroot", "Run broot" },
    },

    n = {
        name = "+navigation",
        t = { ":BTags", "Tags in current buffer" },
        T = { ":Tags", "Tags in the project (ctags -R)" },
        m = { ":Marks", "Marks" },
    },

    t = {
        name = "+terminal",
        h = { ":split | resize 40 | term", "Open terminal in hsplit" },
        v = { ":vsplit | resize 50 | term", "Open terminal in vsplit" },
        x = { ":bd!", "Close terminal" },
        t = { ":FTcenter", "Centered scratch terminal" },
    },

    w = {
        name = "+window",
        q = { ":ToggleQuickfixList<CR>", "Toggle QuickFix List" },
        o = { ":only", "Close other windows, keep buffers open" },
        x = { ":bd", "Close current buffer" },
        X = { ":bd!", "Force close current buffer" },
        s = { ":bp\\|bd #", "Close current buffer, keep split" },
        S = { ":bp\\|bd! #", "Force close current buffer, keep split" },
    },

    r = {
        name = "+registers",
        p = { ":FZFNeoyank", "Neo yank" },
        P = { ':FZFNeoyank " P', "Neo yank paste" },
    },

    q = {
        name = "+search",
        g = { ":Rg", "RipGrep" },
        h = { ":History:<CR>", "Command history" },
        r = { ":%s/\\<<C-r><C-w>\\>//g<Left><Left>", "Replace word under cursor" },
    },

    c = {
        name = "+code",
        f = { ":Format", "format buffer" },
    }
    },
    {
        prefix = "<leader>",
    }
  )


-- ----------------------------------- FZF ---------------------------------- --
-- Configure FZF to use a floating window configuration
vim.env.FZF_DEFAULT_OPTS = '--layout=reverse'

vim.g.fzf_layout = {
    window = {
      width = 0.9,
      height = 0.8
    }
  }

vim.g.fzf_colors  = {
    ['fg'] = { 'fg', 'Normal' },
    ['bg'] = { 'bg', 'Normal' },
    ['hl'] = { 'fg', 'Comment' },
    ['fg+'] = { 'fg', 'CursorLine' },
    ['bg+'] = { 'bg', 'Normal' },
    ['hl+'] = { 'fg', 'Statement' },
    ['info'] = { 'fg', 'PreProc' },
    ['border'] = { 'fg', 'CursorLine' },
    ['prompt'] = { 'fg', 'Conditional' },
    ['pointer'] = { 'fg', 'Exception' },
    ['marker'] = { 'fg', 'Keyword' },
    ['spinner'] = { 'fg', 'Label' },
    ['header'] = { 'fg', 'Comment' }
}

-- Set FZF options for commits log
vim.g.fzf_commits_log_options = '--graph --color=always ' ..
      '--format="%C(yellow)%h%C(red)%d%C(reset) ' ..
      '- %C(bold green)(%ar)%C(reset) %s %C(blue)<%an>%C(reset)"'


-- Set FZF preview command
vim.env.FZF_PREVIEW_COMMAND =
      '(bat --color=always --theme=base16 --style=numbers --pager=never --line-range=$FIRST:$LAST --highlight-line=$CENTER {} ' ..
      '|| highlight -O ansi --force  -l {} ' ..
      '|| cat {})' ..
      '2> /dev/null | head -100'

-- Use preview for files
vim.api.nvim_create_user_command(
    'Files',
    function(arg)
        vim.call('fzf#vim#files', arg.qargs, vim.call('fzf#vim#with_preview'), arg.bang)
    end,
    { bang = true, nargs = "?", complete = "dir" } )

-- Use preview for ripgrep
vim.api.nvim_create_user_command(
    'Rg',
    function(arg)
        local query = '""'
        if arg.qargs ~= nil then
            query = vim.fn.shellescape(arg.qargs)
        end

        vim.call('fzf#vim#grep', 'rg --column --line-number --no-heading --color=always --smart-case -- ' .. query,
            1, vim.call('fzf#vim#with_preview'), arg.bang)
    end,
    { bang = true, nargs = '*'})


-- -------------------------------- floaterm -------------------------------- --
vim.g.floaterm_position = 'bottom'
vim.g.floaterm_width = 0.98
vim.g.floaterm_height = 0.35

-- Map ALT+T to toggle floating terminal
vim.keymap.set('n', '<A-t>', ':FloatermToggle<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<A-t>', '<Esc>:FloatermToggle<CR>', { noremap = true, silent = true })
vim.keymap.set('t', '<A-t>', '<C-\\><C-n>:FloatermToggle<CR>', { noremap = true, silent = true })

-- Map double ALT+T to kill floating terminal
vim.keymap.set('n', '<A-t><A-t>', ':FloatermKill<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<A-t><A-t>', '<Esc>:FloatermKill<CR>', { noremap = true, silent = true })
vim.keymap.set('t', '<A-t><A-t>', '<C-\\><C-n>:FloatermKill<CR>', { noremap = true, silent = true })

-- Map ALT+T,N to next floating terminal
vim.keymap.set('n', '<A-t><A-n>', ':FloatermNext<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<A-t><A-n>', '<Esc>:FloatermNext<CR>', { noremap = true, silent = true })
vim.keymap.set('t', '<A-t><A-n>', '<C-\\><C-n>:FloatermNext<CR>', { noremap = true, silent = true })

-- Map ALT+T,P to previous floating terminal
vim.keymap.set('n', '<A-t><A-p>', ':FloatermPrev<CR>', { noremap = true, silent = true })
vim.keymap.set('i', '<A-t><A-p>', '<Esc>:FloatermPrev<CR>', { noremap = true, silent = true })
vim.keymap.set('t', '<A-t><A-p>', '<C-\\><C-n>:FloatermPrev<CR>', { noremap = true, silent = true })

-- Define commands for specific floating terminals using nvim_create_user_command
local function defineFloatermCommand(name, floatermName, commandArgs)
    local command = string.format("FloatermNew --name=%s %s", floatermName, commandArgs)

    vim.api.nvim_create_user_command(name,
      command,
      {nargs = "*", complete = "customlist," .. name})
  end

defineFloatermCommand("FTcenter", "scratch", "--width=0.8 --height=0.8 --position=center --autoclose=1")
defineFloatermCommand("FTlazygit", "lazygit", "--width=0.8 --height=0.8 --position=center --autoclose=1 lazygit")
defineFloatermCommand("FTtig", "tig", "--width=0.8 --height=0.8 --position=center --autoclose=1 tig")
defineFloatermCommand("FTbroot", "broot", "--width=0.8 --height=0.8 --position=center --autoclose=1 broot -g -i")
defineFloatermCommand("FTranger", "ranger", "--width=0.8 --height=0.8 --position=center --autoclose=1 ranger")


-- ------------------------------- IndentLine ------------------------------- --
-- Set the concealcursor option
vim.g.indentLine_concealcursor = ''

-- Set the default highlight group
vim.g.indentLine_defaultGroup = 'SpecialKey' -- Use the same highlight group as for listchars TAB

-- Set the characters for indent lines
vim.g.indentLine_char = '│'
vim.g.indentLine_first_char = '│'


-- -------------------------------- gitgutter ------------------------------- --
vim.g.gitgutter_map_keys = 0 -- no default keybindings


-- ------------------------------ git-messenger ----------------------------- --
vim.api.nvim_set_hl(0, "gitmessengerHash", { link = "PreProc" })
