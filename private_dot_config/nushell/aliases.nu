# (n)vim
alias nv = nvim
alias vim = vim -u ~/.vimrc

# eza
alias e = eza -lg --time-style=iso
alias ea = eza -lga --time-style=iso
alias et = eza -lgT --time-style=iso
alias eg = eza -lga --time-style=iso --git --git-ignore
alias ed = eza -lga --time-style=long-iso --sort=date -r

# zoxide
alias d = zi

# k8s
alias ktl = kubectl
alias ktx = kubectx
alias kns = kubens
alias mktl = minikube kubectl --
alias kex = kubectl-explore

# terraform
alias tf = tofu
alias tg = terragru
