# Nushell Environment Config File
#
# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# $env.PATH = ($env.PATH | split row (char esep) | prepend '/some/path')
# An alternate way to add entries to $env.PATH is to use the custom command `path add`
# which is built into the nushell stdlib:
# use std "path add"
# $env.PATH = ($env.PATH | split row (char esep))
# path add /some/path
# path add ($env.CARGO_HOME | path join "bin")
# path add ($env.HOME | path join ".local" "bin")
# $env.PATH = ($env.PATH | uniq)

# To load from a custom file you can use:
# source ($nu.default-config-dir | path join 'custom.nu')

# CARAPACE
$env.CARAPACE_BRIDGES = 'zsh,fish,bash,inshellisense' # optional
mkdir ~/.cache/carapace
carapace _carapace nushell | save --force ~/.cache/carapace/init.nu

# ATUIN
mkdir ~/.local/share/atuin/
atuin init nu | save --force ~/.local/share/atuin/init.nu

# ZOXIDE
zoxide init nushell | save --force ~/.zoxide.nu

#OH-MY-POSH
mkdir ~/.cache/oh-my-posh
oh-my-posh init nu --config ~/.config/oh-my-posh/oh-my-posh.omp.yaml --print | save --force ~/.cache/oh-my-posh/init.nu
