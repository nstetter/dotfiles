# Nushell Config File
#

$env.config = {
    show_banner: false
    edit_mode: vi # vi or emacs
    use_kitty_protocol: true
}

source ($nu.default-config-dir | path join 'aliases.nu')

# CARAPACE
source ~/.cache/carapace/init.nu

# ATUIN
source ~/.local/share/atuin/init.nu

# ZOXIDE
source ~/.zoxide.nu

# OH-MY-POSH
source ~/.cache/oh-my-posh/init.nu
