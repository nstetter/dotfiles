-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- ---------------------------------- Misc ---------------------------------- --
config.set_environment_variables = {
  PATH = '/usr/local/bin:/opt/homebrew/bin:' .. os.getenv('PATH')
}
config.default_prog = { 'zellij' }

-- 'swap' left and right alt keys
config.send_composed_key_when_left_alt_is_pressed = false
config.send_composed_key_when_right_alt_is_pressed = true

config.hide_tab_bar_if_only_one_tab = true

config.audible_bell = "Disabled"

config.window_padding = {
  left = 2,
  right = 2,
  top = 5,
  bottom = 0,
}

window_decorations = "RESIZE"

-- ---------------------------------- Theme --------------------------------- --
-- replace 'black' in colorscheme
local OneDark16Color = wezterm.color.get_builtin_schemes()['OneDark (base16)'];
OneDark16Color.ansi[1] = '#5c6370'

-- copy from altered palette to own scheme
config.color_schemes = {
  ['MyOneDark16'] = OneDark16Color,
}
config.color_scheme = 'MyOneDark16'

config.colors = {
  background = '#282c34',
}

-- ---------------------------------- Font ---------------------------------- --
config.font = wezterm.font_with_fallback {
  {
    family = 'FiraCode Nerd Font',
    weight = 'Regular',
    harfbuzz_features = { 'calt=1', 'clig=1', 'liga=1' },
  },
  {
    family = "DejaVu Sans Mono",
    scale = 1.2,
  },
}
config.font_size = 14
config.freetype_load_target = "Light"
-- config.freetype_render_target = "HorizontalLcd"
config.bold_brightens_ansi_colors = "BrightAndBold"
config.allow_square_glyphs_to_overflow_width = "WhenFollowedBySpace"
config.default_cursor_style = 'SteadyBlock'

-- and finally, return the configuration to wezterm
return config
