# ---------------------------------------------------------------------------- #
#                                    fzf-git                                   #
# ---------------------------------------------------------------------------- #
#https://junegunn.kr/2016/07/fzf-git/
is_in_git_repo() {
  git rev-parse HEAD > /dev/null 2>&1
}

fzf-down() {
fzf --height 50% "$@" --border
}

fzf_gf() {
  is_in_git_repo || return
  git -c color.status=always status --short |
    fzf-down -m --ansi --nth 2..,.. \
    --preview '(git diff --color=always -- {-1} | sed 1,4d; cat {-1}) | head -500' |
    cut -c4- | sed 's/.* -> //'
  }

fzf_gb() {
  is_in_git_repo || return
  git branch -a --color=always | grep -v '/HEAD\s' | sort |
    fzf-down --ansi --multi --tac --preview-window right:70% \
    --preview 'git log --oneline --graph --date=short --pretty="format:%C(auto)%cd %h%d %s" $(sed s/^..// <<< {} | cut -d" " -f1) -- | head -'$LINES |
    sed 's/^..//' | cut -d' ' -f1 |
    sed 's#^remotes/##'
  }

fzf_gt() {
  is_in_git_repo || return
  git tag --sort -version:refname |
    fzf-down --multi --preview-window right:70% \
    --preview 'git show --color=always {} | head -'$LINES
  }

fzf_gh() {
  is_in_git_repo || return
  git log --exclude=refs/stash --all --date=short --format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)" --graph --color=always |
    fzf-down --ansi --no-sort --reverse --multi --bind 'ctrl-s:toggle-sort' \
    --header 'Press CTRL-S to toggle sort' \
    --preview 'grep -o "[a-f0-9]\{7,\}" <<< {} | head -1 | xargs git show --color=always | head -'$LINES |
    grep -o "[a-f0-9]\{7,\}" | head -1
  }

fzf_gr() {
  is_in_git_repo || return
  git remote -v | awk '{print $1 "\t" $2}' | uniq |
    fzf-down --tac \
    --preview 'git log --oneline --graph --date=short --pretty="format:%C(auto)%cd %h%d %s" --remotes={1} | head -200' |
    cut -d$'\t' -f1
  }

join-lines() {
local item
while read item; do
  echo -n "${(q)item} "
done
}
bind-git-helper() {
local c
for c in $@; do
  eval "fzf-g$c-widget() { local result=\$(fzf_g$c | join-lines); zle reset-prompt; LBUFFER+=\$result }"
  eval "zle -N fzf-g$c-widget"
  eval "bindkey '^[g^[$c' fzf-g$c-widget"
done
}
bind-git-helper f b t r h
unset -f bind-git-helper


# ---------------------------------------------------------------------------- #
#                                      git                                     #
# ---------------------------------------------------------------------------- #
# list git/gerrit changes
fzf_git_changes() {
  local ref
  ref=$(git for-each-ref --color=always --sort=committerdate --format="%(refname:short) %09 %(subject)" | grep -v 'meta' \
    | fzf-down --tac --preview-window right:50% \
     --preview 'git show --color=always {1} | head -'$LINES)
  #ref=$(git show-ref | cut -d' ' -f2 | fzf-down --tac --preview-window right:70% \
  #  --preview 'git show --color=always {} | head -'$LINES)
      if [[ -n ${ref} ]]; then
        ref=$(cut -f1 <<<"$ref")
        ref=$(tr -d '[:space:]' <<<"$ref")
        LBUFFER+="$ref"
        zle redisplay
      fi
}
zle -N fzf_git_changes
bindkey '^[g^[c' fzf_git_changes

fzf_fetch_changes() {
  local remote
  remote=$(git remote | fzf-down --tac --preview-window right:70% \
    --preview 'git remote show {} | head -'$LINES)
      if [[ -n ${remote} ]]; then
        git fetch ${remote} "+refs/changes/*:refs/remotes/origin/changes/*"
      fi
}
alias fetchg='fzf_fetch_changes'

# Open conflicted files in editor
fzf-conflicts() {
  local conflict
  conflict=$(git diff --name-only --diff-filter=U | fzf-down --tac --preview-window right:70% \
    --preview 'git diff --color=always $(git rev-parse HEAD) -- {} | head -'$LINES)
    if [[ -n ${conflict} ]]; then
      $EDITOR ${conflict}
    fi
}
alias gconf='fzf-conflicts'

# push changes to git remote, sensitive about code-review
fzf-push() {
  local remotebranch
  local remote
  local branch
  local review=false

  #check if there are branches named */changes*, if yes we use code-review
  if git branch -r --list | grep -q changes; then
    review=true
  fi

  remotebranch=$(git branch -r --list --sort=-committerdate | grep -v "changes\|->" | sed 's/ *//' | \
   fzf-down --preview-window right:70% \
   --preview 'git show --color=always {} | head -'$LINES)

  if [[ -n ${remotebranch} ]]; then
    remote=$(cut -d'/' -f1 <<<"$remotebranch")
    branch=$(cut -d'/' -f2 <<<"$remotebranch")
    if $review; then
      LBUFFER+="git push ${remote} HEAD:refs/for/${branch}"
    else
      LBUFFER+="git push ${remote} ${branch}"
    fi
  fi
  zle redisplay
}
zle -N fzf-push
bindkey '^[g^[p' fzf-push

#get hashes from reflogs
fzf-reflog() {
  local comhash

  comhash=$(git reflog --color=always| fzf-down --ansi --delimiter ' ' \
   --preview-window right:50%  \
   --preview "(git show --color=always {1} | head -$LINES)" \
   | awk '{print $1}')

  if [[ -n ${comhash} ]]; then
    LBUFFER+="${comhash}"
  fi
  zle redisplay
}
zle -N fzf-reflog
bindkey '^[g^[r' fzf-reflog


# ---------------------------------------------------------------------------- #
#                                     files                                    #
# ---------------------------------------------------------------------------- #
# find and edit files
fzf_find_edit() {
  file=($(fzf ${FZF_FILE_PREVIEW}))
  if [[ -n ${file} ]]; then
    $EDITOR ${file}
  fi
}
alias fze='fzf_find_edit'

# find term inside file and open file
fzf_rg_edit(){
  if [[ $# == 0 ]]; then
    echo 'Error: search term was not provided.'
    return
  fi
  local match=$(
  rg --color=never --line-number "$1" |
    fzf --no-multi --delimiter : \
    --preview "(bat $BAT_DEFAULTS --line-range {2}: {1} || rg --pretty --context 10 {3} {1} || highlight -O ansi --force -l {1} || echo no preview available) 2> /dev/null"
      )
      local file=$(echo "$match" | cut -d':' -f1)
      if [[ -n $file ]]; then
        $EDITOR $file +$(echo "$match" | cut -d':' -f2)
      fi
}
alias fzg='fzf_rg_edit'

# ALT-I - search through ~
fzf-locate-widget() {
local selected
#if selected=$(locate / | fzf -q "$LBUFFER"); then
  if selected=$(fd . ${HOME} | fzf --no-multi ${FZF_FILE_PREVIEW}); then
    LBUFFER+=$selected
  fi
  zle redisplay
}
zle     -N    fzf-locate-widget
bindkey '^[i' fzf-locate-widget


# ---------------------------------------------------------------------------- #
#                                     dirs                                     #
# ---------------------------------------------------------------------------- #
# find and files and enter directory of file
fzf_find_cd() {
  file=$(fzf --no-multi ${FZF_FILE_PREVIEW})
  if [[ -n ${file} ]]; then
    cd $(dirname ${file})
  fi
}
alias fzc='fzf_find_cd'

# select parent directory, ALT-P
  fzf_parent_widget() {
    local dirs=()
    local parent_dir

    get_parent_dirs() {
      if [[ -d "$1" ]]; then dirs+=("$1"); else return; fi
      if [[ "$1" == '/' ]]; then
        for _dir in "${dirs[@]}"; do echo "$_dir"; done
      else
        get_parent_dirs "$(dirname "$1")"
      fi
    }

  parent_dir="$(
  get_parent_dirs "$(realpath "${1:-$PWD}")" \
    | fzf +m
      )" || return

  #cd "$parent_dir" || return
  LBUFFER+="$parent_dir" || return
  zle redisplay
}
zle -N fzf_parent_widget
bindkey '^[p' fzf_parent_widget


# ---------------------------------------------------------------------------- #
#                                    gcloud                                    #
# ---------------------------------------------------------------------------- #
# switch gcloud projects with fzf (https://anakaiti.net/posts/gcloud-fzf/)
gtx(){
    local readonly g=( "gcloud" "config" "configurations" )
    ${g} activate ${1:-$(${g} list | fzf-down --tac --header-lines 1 | cut -d ' ' -f 1)}
}

# ---------------------------------------------------------------------------- #
#                                      k8s                                     #
# ---------------------------------------------------------------------------- #
# tail logs for all containers in a deployment using stern
function fzf_stern_container() {
  local selected_deployment=$(kubectl get deployments --all-namespaces | fzf-down --header-lines=1 --prompt="Select a Deployment: " --preview="kubectl describe deployment {2} -n {1} | bat $BAT_DEFAULTS --style numbers -l yaml")

  if [[ -z "$selected_deployment" ]]; then
    echo "No deployment selected, exiting."
    return 1
  fi

  local namespace=$(echo $selected_deployment | awk '{print $1}')
  local deployment_name=$(echo $selected_deployment | awk '{print $2}')

  local containers=$(kubectl get deployment $deployment_name -n $namespace -o jsonpath='{.spec.template.spec.containers[*].name}' | tr ' ' '\n')

  if [[ -z "$containers" ]]; then
    echo "No containers found in the selected deployment, exiting."
    return 1
  fi

  local selected_container=$(echo "$containers" | fzf-down --prompt="Select a Container: ")

  if [[ -z "$selected_container" ]]; then
    echo "No container selected, exiting."
    return 1
  fi

  stern $deployment_name --namespace $namespace --container $selected_container
}
zle -N fzf_stern_container
bindkey '^[k^[s' fzf_stern_container
