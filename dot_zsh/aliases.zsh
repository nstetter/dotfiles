# always use neovim when installed
#if type nvim > /dev/null 2>&1; then
#  if [ $(nvim --version | sed -n "s/^NVIM\sv[0-9]\+\.\([0-9]\+\).*$/\1/p") -ge 4 ]; then
#    alias vim='nvim'
#  fi
#else
#  # use only private vimrc
#  alias vim="vim -u ~/.vimrc"
#fi

# (n)vim
alias nv='nvim'
alias wiki='nvim +VimwikiIndex'
# use only private vimrc
alias vim="vim -u ~/.vimrc"

# VS Code
alias co='code --disable-workspace-trust -n'

# common/builtins
alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
alias ll='ls -l'      #long list

alias grep='grep --color'

alias t='tail -f'

alias gitcurl='curl -LJO'

alias hl='highlight -O ansi --force'

alias yless='jless --yaml'

#print full file path
alias fp='readlink -f'

#xsel
alias clip='xsel --clipboard'
alias sel='xsel --primary'

#reload directory
reload_dir() {
	cd && - >/dev/null;
	zle redisplay
}
zle -N reload_dir
bindkey '^[r' reload_dir

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'

# eza
alias e='eza -lg --time-style=iso'
alias ea='eza -lga --time-style=iso'
alias et='eza -lgT --time-style=iso'
alias eg='eza -lga --time-style=iso --git --git-ignore'
alias ed='eza -lga --time-style=long-iso --sort=date -r'

# fd
type -p fdfind > /dev/null && alias fd='fdfind'     #in debian fd binary is called fdfind
alias fda='fd -IH' #no ignore, search hidden

# enter dir and li
cd_ls() {
	cd "$@" && eza -lg --time-style=iso
}
alias ce='cd_ls'

# bat
type -p batcat > /dev/null && alias bat='batcat' # macos bat binary is called batcat
alias rat='bat --theme=base16'

# git
# alias gc='git commit'
alias gca='git commit --amend'
alias gcm='git commit -m '
# alias ga='git add'
# alias gchk='git checkout'
# alias gl='git log --oneline -n 15'
# alias gla='git log --all --graph --decorate --oneline' #don't open older tags with --simplify-by-decoration
# alias gp='git push'
alias gcap='git commit --amend --no-edit && git push --force-with-lease'
alias gpo='git push -u origin $(git rev-parse --abbrev-ref HEAD)' #push current branch to origin

# zoxide
alias d='zi'

# ip
alias ip='ip -c'
alias ipls='ip -br a'

# tig
alias tiga='tig --all --invert-grep' #show all branches, excluding auto-commits from gerrit

# vifm
alias vf='vifm'

# k8s
alias ktl='kubectl'
alias ktx='kubectx'
alias kns="kubens"
alias mktl='minikube kubectl -- '
alias kex='kubectl-explore'

# terraform
alias tf='tofu'
alias tg='terragrunt'

# gcloud
alias gsh='gcloud compute ssh'
alias gshi='gcloud compute ssh --internal-ip'
alias gsht='gsh --tunnel-through-iap'
