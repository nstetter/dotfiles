# number conversion
function bin2hex {
  local hex=$(([##16]2#$1))
  echo "HEX: 0x$hex"
}
function dec2bin {
  local bin=$(([##2]$1))
  echo "BIN: $bin"
}
function dec2hex {
  local hex=$(([##16]$1))
  echo "HEX: 0x$hex"
}
function bin2dec {
 local dec=$((2#$1))
 echo "DEC: $dec"
}
function hex2bin {
  local bin=$(([##2]16#$1))
  echo "BIN: $bin"
}
function hex2dec {
  local dec=$((16#$1))
  echo "DEC: $dec"
}

# join mp3-files
function audio-join {
  ffmpeg -i "concat:${(j:|:)@[2,-1]}" -acodec copy $1
}
#usage: audio-join output.mp3 *.mp3

# create new shell script and start editing
function newscript () {
	local ext='.sh'
	if [[ -z $1 ]]; then
		echo "provide a filename"
	elif [[ -e $1$ext ]]; then
		echo "file already exists"
	else
		echo "#!/bin/bash" > ./$1$ext
		chmod +x ./$1$ext
		nvim '+normal Go' +startinsert $1$ext
	fi
}

# start debugging container in k8s
function kubedebug () {

  local NAMESPACE="default"
  local PODNAME="debug-pod-norman"
  local IMAGE="praqma/network-multitool"

  local OPTIND
  while getopts ':n:h' opt
  do
    case "$opt" in
      n)
        NAMESPACE="$OPTARG"
        ;;
      ?|h)
        echo "Usage: kubedebug [-n <namespace>]"
        echo "       If -n is ommited namespace 'default' is used"
        return 1
        ;;
    esac
  done
  shift "$(($OPTIND -1))"

  echo "Running debug-pod '$PODNAME' in Namespace '$NAMESPACE'\n"

  if kubectl get pod $PODNAME -n $NAMESPACE > /dev/null 2>&1; then
    kubectl exec -i --tty -n $NAMESPACE $PODNAME -- sh
  else
    kubectl run --restart=Never -n $NAMESPACE --rm -i --tty $PODNAME --image=$IMAGE -- sh
  fi
}

# convert yaml to hcl
function yaml2hcl () {
	if [[ -z $1 ]]; then
		echo "provide a filename"
    else
        echo -e "yamldecode(file(\"${1}\"))" | terraform console
    fi
}

# print named terminal colors https://gist.github.com/netzverweigerer/08f423d4111bb2c5c53d
function colors-named () {
  print "
  \e[1m\e[4mFOREGROUND:\e[0m \e[39m39:default\e[0m, \e[30m30:black\e[0m, \e[31m31:red\e[0m, \e[32m32:green\e[0m, \e[33m33:yellow\e[0m, \e[34m34:blue\e[0m, \e[35m35:magenta\e[0m, \e[36m36:cyan\e[0m, \e[37m37:light gray\e[0m, \e[90m90:dark gray\e[0m, \e[91m91:light red\e[0m, \e[92m92:light green\e[0m, \e[93m93:light yellow\e[0m, \e[94m94:light blue\e[0m, \e[95m95:light magenta\e[0m, \e[96m96:light cyan\e[0m, \e[97m97:white\e[0m

  \e[1m\e[4mBACKGROUND:\e[0m \e[49m49:default\e[0m, \e[40m40:black\e[0m, \e[41m41:red\e[0m, \e[42m42:green\e[0m, \e[43m43:yellow\e[0m, \e[44m44:blue\e[0m, \e[45m45:magenta\e[0m, \e[46m46:cyan\e[0m, \e[47m47:light gray\e[0m, \e[100m100:dark gray\e[0m, \e[101m101:light red\e[0m, \e[102m102:light green\e[0m, \e[103m103:light yellow\e[0m, \e[104m104:light blue\e[0m, \e[105m105:light magenta\e[0m, \e[106m106:light cyan\e[0m, \e[107m107:white\e[0m

  \e[1m\e[4mFORMATTING:\e[0m \e[1m1:bold\e[0m, \e[2m2:dim\e[0m, \e[4m4:underlined\e[0m, \e[5m5:blink\e[0m, \e[7m7:inverted\e[0m, \e[8m8:hidden\e[0m
  "
}

# print terminal 255 colors palette
function color-palette () {
    for i in {0..255}; do print -Pn "%K{$i}  %k%F{$i}${(l:3::0:)i}%f " ${${(M)$((i%6)):#3}:+$'\n'}; done
}

# decode JWT token
jwtd() {
    if [[ -x $(command -v jq) ]]; then
         jq -R 'split(".") | .[0],.[1] | @base64d | fromjson' <<< "${1}"
         echo "Signature: $(echo "${1}" | awk -F'.' '{print $3}')"
    fi
}

function k8s_cpu_requests() {
  kubectl get pods --all-namespaces -o json | jq -r '
    [
      .items[] |
      {
        namespace: .metadata.namespace,
        deployment_statefulset_daemonset: (
          .metadata.ownerReferences[]? |
          select(.kind=="ReplicaSet" or .kind=="StatefulSet" or .kind=="DaemonSet") |
          .name | sub("-[a-z0-9]+$";"")
        ),
        resource_type: (
          .metadata.ownerReferences[]? |
          select(.kind=="ReplicaSet" or .kind=="StatefulSet" or .kind=="DaemonSet") |
          .kind
        ),
        pod: .metadata.name,
        cpu_request: (.spec.containers[].resources.requests.cpu // "0")
      }
    ]
    | group_by(.deployment_statefulset_daemonset)
    | map({
        deployment_statefulset_daemonset: .[0].deployment_statefulset_daemonset,
        resource_type: .[0].resource_type,
        namespace: .[0].namespace,
        pods: map({pod: .pod, cpu_request: .cpu_request}),
        total_cpu_request_cores: (
          map(
            .cpu_request |
            if test("m$") then sub("m$";"") | tonumber / 1000
            elif test("^[0-9]+$") then tonumber
            else 0 end
          ) | add
        )
      })
    | map(
        .total_cpu_request_cores = (.total_cpu_request_cores * 1000 | floor / 1000)  # Round to 3 decimal places
      )
    | sort_by(-.total_cpu_request_cores)
    | (["NAMESPACE", "RESOURCE", "RESOURCE_TYPE", "TOTAL_CPU_REQUEST(cores)"] | @tsv),
      (.[] | [.namespace, .deployment_statefulset_daemonset, .resource_type, (.total_cpu_request_cores | tostring)] | @tsv)' | column -t
}


function k8s_memory_requests() {
  kubectl get pods --all-namespaces -o json | jq -r '
    [
      .items[] |
      {
        namespace: .metadata.namespace,
        deployment_statefulset_daemonset: (
          .metadata.ownerReferences[]? |
          select(.kind=="ReplicaSet" or .kind=="StatefulSet" or .kind=="DaemonSet") |
          .name | sub("-[a-z0-9]+$";"")
        ),
        resource_type: (
          .metadata.ownerReferences[]? |
          select(.kind=="ReplicaSet" or .kind=="StatefulSet" or .kind=="DaemonSet") |
          .kind
        ),
        pod: .metadata.name,
        mem_request: (.spec.containers[].resources.requests.memory // "0")
      }
    ]
    | group_by(.deployment_statefulset_daemonset)
    | map({
        deployment_statefulset_daemonset: .[0].deployment_statefulset_daemonset,
        resource_type: .[0].resource_type,
        namespace: .[0].namespace,
        pods: map({pod: .pod, mem_request: .mem_request}),
        total_mem_request_gib: (
          map(
            .mem_request |
            if test("Ki$") then sub("Ki$";"") | tonumber * 0.000000953674
            elif test("Mi$") then sub("Mi$";"") | tonumber * 0.0009765625
            elif test("Gi$") then sub("Gi$";"") | tonumber * 1
            elif test("Ti$") then sub("Ti$";"") | tonumber * 1024
            elif test("M$") then sub("M$";"") | tonumber * 0.000953674   # Handle '50M'
            elif test("G$") then sub("G$";"") | tonumber * 1             # Handle '2G'
            elif test("T$") then sub("T$";"") | tonumber * 1024          # Handle '1T'
            elif test("^[0-9]+$") then tonumber * 1                      # Assume raw numbers are bytes
            else 0 end
          ) | add
        )
      })
    | map(
        .total_mem_request_gib = (.total_mem_request_gib * 1000 | floor / 1000)  # Round to 3 decimal places
      )
    | sort_by(-.total_mem_request_gib)
    | (["NAMESPACE", "RESOURCE", "RESOURCE_TYPE", "TOTAL_MEMORY_REQUEST(GiB)"] | @tsv),
      (.[] | [.namespace, .deployment_statefulset_daemonset, .resource_type, (.total_mem_request_gib | tostring)] | @tsv)' | column -t
}
