# Dotfiles

These are mydotfiles, managed with [chezmoi](https://www.chezmoi.io)

## Howto use

### Install chezmoi

`sh -c "$(curl -fsLS git.io/chezmoi)"`

### Init dotfiles

```
chezmoi init git@gitlab.com:nstetter/dotfiles.git
chezmoi apply
```

### Update

```
chezmoi update
```
