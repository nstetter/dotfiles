tap "homebrew/cask"
# base
brew "curlie"
brew "git"
brew "git-delta"
brew "htop"
brew "neovim"
brew "wget"
cask "wezterm"
brew "zellij"

# fonts
tap "homebrew/cask-fonts"
cask "font-fira-code-nerd-font"
cask "font-dejavu-sans-mono-nerd-font"

# keyboard
tap "ohueter/tap"
brew "ohueter/tap/autokbisw", restart_service: true

# productivity
brew "atuin"
brew "bat"
brew "broot"
brew "chezmoi"
brew "clipboard"
brew "dive"
brew "doggo"
brew "eza"
brew "fd"
brew "fzf"
brew "jid"
brew "jq"
brew "lazygit"
brew "pre-commit"
brew "ripgrep"
brew "mise"
brew "sd"
brew "tldr"
brew "vifm"
brew "yq"
brew "zoxide"

# misc
brew "aichat"
brew "vault", restart_service: true

# ci/cd
brew "act"

# GNU
brew "gawk"
brew "gnu-sed"
brew "gnu-tar"
brew "gnupg"
brew "gnutls"
brew "grep"

# K8S
brew "k9s"
brew "kubectx"
brew "kubent"

# Terraform
brew "infracost"
brew "tflint"
brew "tilt"


# casks
cask "1password"
cask "google-cloud-sdk"
cask "keepassxc"
cask "orbstack"
cask "slack"
cask "spotify"
cask "visual-studio-code"
cask "vivaldi"
cask "vlc"

# VSCode
vscode "eamodio.gitlens"
vscode "GitHub.copilot"
vscode "golang.go"
vscode "Grafana.vscode-jsonnet"
vscode "hashicorp.hcl"
vscode "jakearl.search-editor-apply-changes"
vscode "johnpapa.vscode-peacock"
vscode "ms-azuretools.vscode-docker"
vscode "ms-kubernetes-tools.vscode-kubernetes-tools"
vscode "PKief.material-icon-theme"
vscode "redhat.vscode-yaml"
vscode "searKing.preview-vscode"
vscode "stackbreak.comment-divider"
vscode "streetsidesoftware.code-spell-checker"
vscode "streetsidesoftware.code-spell-checker-german"
vscode "vscodevim.vim"
vscode "VSpaceCode.whichkey"
vscode "zhuangtongfa.material-theme"
